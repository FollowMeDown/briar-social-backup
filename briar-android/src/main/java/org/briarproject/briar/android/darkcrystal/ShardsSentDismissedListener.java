package org.briarproject.briar.android.darkcrystal;

import androidx.annotation.UiThread;

public interface ShardsSentDismissedListener {

	@UiThread
	void shardsSentDismissed();

}