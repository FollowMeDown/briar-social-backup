package org.briarproject.briar.android.contactselection;

import android.view.View;

import org.briarproject.bramble.api.nullsafety.NotNullByDefault;
import org.briarproject.briar.android.contact.BaseContactListAdapter.OnContactClickListener;

import javax.annotation.Nullable;

import androidx.annotation.UiThread;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

@UiThread
@NotNullByDefault
class SelectableContactHolder
		extends BaseSelectableContactHolder<SelectableContactItem> {

	private boolean alwaysChecked;

	SelectableContactHolder(View v) {
		super(v);
		this.alwaysChecked = false;
	}

	SelectableContactHolder(View v, boolean alwaysChecked) {
		super(v);
		this.alwaysChecked = alwaysChecked;
	}

	@Override
	protected void bind(SelectableContactItem item, @Nullable
			OnContactClickListener<SelectableContactItem> listener) {
		if (alwaysChecked) {
			item.setSelected(true);
		}
		super.bind(item, listener);

		if (item.isDisabled()) {
			info.setVisibility(VISIBLE);
		} else {
			info.setVisibility(GONE);
		}
	}

}
