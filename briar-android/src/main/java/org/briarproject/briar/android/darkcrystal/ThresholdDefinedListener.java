package org.briarproject.briar.android.darkcrystal;

import androidx.annotation.UiThread;

public interface ThresholdDefinedListener {

	@UiThread
	void thresholdDefined();

}