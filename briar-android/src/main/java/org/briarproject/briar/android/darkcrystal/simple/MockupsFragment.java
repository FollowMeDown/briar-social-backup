package org.briarproject.briar.android.darkcrystal.simple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.briarproject.briar.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static java.util.Objects.requireNonNull;

public class MockupsFragment extends SimpleFragment {

    public static final String TAG = MockupsFragment.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireNonNull(getActivity()).setTitle(R.string.title_mockups);
        hasUpNavigation = false;
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mockups,
                container, false);

        /*
         * Backup creation
         */
        view.findViewById(R.id.buttonSelectThreshold).setOnClickListener(e -> {
            show(new ThresholdSelectorFragment());
        });
        view.findViewById(R.id.buttonShardsSent).setOnClickListener(e -> {
            show(new ShardsSentFragment());
        });

        /*
         * Setup
         */
        view.findViewById(R.id.buttonWelcome).setOnClickListener(e -> {
            show(new WelcomeFragment());
        });

        /*
         * Recovery from secret owner's point of view
         */
        view.findViewById(R.id.buttonOwnerRecoveryExplainer).setOnClickListener(e -> {
            show(new OwnerRecoveryModeExplainerFragment());
        });
        view.findViewById(R.id.buttonOwnerRecoveryReceivedShard).setOnClickListener(e -> {
            show(new OwnerRecoveryModeReceivedShardFragment());
        });
        view.findViewById(R.id.buttonOwnerRecoveryMain1).setOnClickListener(e -> {
            show(OwnerRecoveryModeMainFragment.newInstance(0));
        });
        view.findViewById(R.id.buttonOwnerRecoveryMain2).setOnClickListener(e -> {
            show(OwnerRecoveryModeMainFragment.newInstance(1));
        });
        view.findViewById(R.id.buttonOwnerRecoveryMain3).setOnClickListener(e -> {
            show(OwnerRecoveryModeRecoveringFragment.newInstance(4));
        });
        view.findViewById(R.id.buttonOwnerRecoveryAccountRecovered).setOnClickListener(e -> {
            show(new OwnerRecoveryModeAccountRecovered());
        });
        view.findViewById(R.id.buttonOwnerRecoveryErrorExplainer).setOnClickListener(e -> {
            show(new OwnerRecoveryModeErrorExplainerFragment());
        });

        /*
         * Recovery from custodian's point of view
         */
        view.findViewById(R.id.buttonCustodianRecoveryExplainer).setOnClickListener(e -> {
            show(new CustodianRecoveryModeExplainerFragment());
        });
        view.findViewById(R.id.buttonCustodianRecoveryErrorExplainer).setOnClickListener(e -> {
            show(new CustodianRecoveryModeErrorExplainerFragment());
        });

        view.findViewById(R.id.buttonCustodianRecoveryDone).setOnClickListener(e -> {
            show(new CustodianRecoveryModeDoneFragment());
        });

        return view;
    }

    private void show(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction =
                fragmentManager.beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
