package org.briarproject.briar.android.darkcrystal.simple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.briarproject.briar.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class OwnerRecoveryModeAccountRecovered extends SimpleFragment {

    public static final String TAG = OwnerRecoveryModeAccountRecovered.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireNonNull(getActivity()).setTitle(R.string.title_recovery_mode);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_recovered,
                container, false);

        return view;
    }


}
