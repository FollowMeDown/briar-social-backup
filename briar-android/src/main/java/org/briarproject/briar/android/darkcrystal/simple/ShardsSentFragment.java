package org.briarproject.briar.android.darkcrystal.simple;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.briarproject.briar.R;
import org.briarproject.briar.android.darkcrystal.ShardsSentDismissedListener;
import org.briarproject.briar.android.darkcrystal.ThresholdDefinedListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static java.util.Objects.requireNonNull;

public class ShardsSentFragment extends SimpleFragment {

    public static final String TAG = ShardsSentFragment.class.getName();

    protected ShardsSentDismissedListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireNonNull(getActivity()).setTitle(R.string.title_distributed_backup);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shards_sent,
                container, false);

        Button button = view.findViewById(R.id.button);
        button.setOnClickListener(e -> {
                listener.shardsSentDismissed();
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ShardsSentDismissedListener) context;
    }

}
