package org.briarproject.briar.android.darkcrystal.simple;

import android.os.Bundle;

import org.briarproject.briar.R;
import org.briarproject.briar.android.darkcrystal.ShardsSentDismissedListener;
import org.briarproject.briar.android.darkcrystal.ThresholdDefinedListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MockupsActivity extends AppCompatActivity
		implements ThresholdDefinedListener, ShardsSentDismissedListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distributed_backup);

		MockupsFragment fragment = new MockupsFragment();

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction =
				fragmentManager.beginTransaction();
		transaction.replace(R.id.fragmentContainer, fragment);
		transaction.commit();
	}

	@Override
	public void thresholdDefined() {
		getSupportFragmentManager().popBackStack();
	}

	@Override
	public void shardsSentDismissed() {
		getSupportFragmentManager().popBackStack();
	}

}
