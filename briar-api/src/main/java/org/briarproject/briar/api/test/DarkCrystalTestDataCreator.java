package org.briarproject.briar.api.test;

import org.briarproject.bramble.api.contact.Contact;
import org.briarproject.bramble.api.db.DbException;
import org.briarproject.bramble.api.lifecycle.IoExecutor;
import org.briarproject.bramble.api.nullsafety.NotNullByDefault;

@NotNullByDefault
public interface DarkCrystalTestDataCreator {

	/**
	 * Create fake test data on the IoExecutor
	 */
	void createTestData();

	@IoExecutor
	Contact addContact(String name) throws DbException;
}
